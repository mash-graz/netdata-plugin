# Helpers to develop [netdata](https://netdata.cloud) plugins in `rust` and webassembly

This crate contains two different levels of access to the netdata 
plugin communication API:

* Low-Level Routines to construct, format  and output the raw netdata commands (`CHART, DIMENSION, BEGIN, SET, END...`)

* and the `Collector`-interface, which[hopefully] simplifies the required control flow handling of netdata plugins and their realization in rust and webassembly.

For more information: [generated documentation](https://mash-graz.gitlab.io/netdata-plugin/netdata_plugin)